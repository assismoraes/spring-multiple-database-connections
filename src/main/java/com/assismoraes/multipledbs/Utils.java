package com.assismoraes.multipledbs;


import java.util.ArrayList;
import java.util.List;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class Utils {

	public static List<String> attrsNames = new ArrayList<String>();
	
	public static void main(String[] args) {

		String query = "query{\n" + 
				"  factAttendances(first : 2\n" + 
				"  where     : {\n" + 
				"    pk_fact_attendance_gt :0\n" + 
				"    situation :\"Aberto(a)\"\n" + 
				"  }) {\n" + 
				"    situation    \n" + 
				"    total_value    \n" + 
				"    fk_dim_beneficiary{      \n" + 
				"      card_number     name      \n" + 
				"      fk_dim_product {        \n" + 
				"        name      \n" + 
				"      }    \n" + 
				"    }     \n" + 
				"  }\n" + 
				"}\n" + 
				"";
		query = "atendimentos {\n" + 
				"    autorizacao\n" + 
				"    valor_total\n" + 
				"    prestador {\n" + 
				"        cnpj\n" + 
				"        nome_do_prestador\n" + 
				"        cidade {\n" + 
				"            nome_da_cidade\n" + 
				"            estado {\n" + 
				"                nome_do_estado\n" + 
				"                abreviacao\n" + 
				"            }\n" + 
				"        }\n" + 
				"    }\n" + 
				"    profissional {\n" + 
				"        cpf\n" + 
				"        nome\n" + 
				"        orgao_expedidor\n" + 
				"    }\n" + 
				"    especialidade {\n" + 
				"        codigo\n" + 
				"        descricao\n" + 
				"    }\n" + 
				"}";
		query = query.replace("\n", "");

		Integer[] filtersPos = Utils.getStartEndPositions(query, '(', ')', 0);
		Integer[] attrsPos = Utils.getStartEndPositions(query, '{', '}', filtersPos[1]);

		String subQueryFields = query.substring(attrsPos[0], attrsPos[1])
				.replace("{", " { ").replace("}", " } ")
				.trim().replaceAll(" +", " ")
				.replace("{", ": {")
				.replace(" :", ":")
				;

		String[] attrs1 = subQueryFields.split(" ");
		String finalQueryFields = "";
		for (int i = 0; i < attrs1.length; i++) {
			if(!attrs1[i].contains(":") && !attrs1[i].contains("}") && !attrs1[i].contains("{")) {
				attrs1[i] = attrs1[i] + ": null, ";
			}
			finalQueryFields += attrs1[i]; 
		}
		finalQueryFields = "{ atendimentos: {" + finalQueryFields + "} }";
		finalQueryFields = finalQueryFields.replace("}", "},");
		finalQueryFields = finalQueryFields.replace(" ", "").replace("null,}", "null}").replace(",}", "}");
		finalQueryFields = finalQueryFields.substring(0, finalQueryFields.length()-1);

		JsonObject jsonObject = new JsonParser().parse(finalQueryFields).getAsJsonObject();

		Utils.setAttrsNames(jsonObject, "data");
		
		for (String attrName : Utils.attrsNames) {
			System.out.println("--> " + attrName);
		}

	}

	
	public static void setAttrsNames(JsonObject jsonObject, String parentName) {
		JsonObject obj = jsonObject;
		List<String> objList = new ArrayList<String>(obj.keySet());
		for (String key : objList) {
			if(obj.get(key).isJsonObject())
				Utils.setAttrsNames(obj.get(key).getAsJsonObject(), parentName + "." + key);
			else {
				Utils.attrsNames.add(parentName + "." + key);
			}
		}
	}

	public static Integer[] getStartEndPositions(String text, Character openChar,  Character closeChar, Integer openPosition) {
		for (int i = openPosition; i < text.length(); i++) {
			if(text.charAt(i) == openChar) {
				Integer open = 1;
				Integer close = 0;
				for (int j = i+1; j < text.length(); j++) {
					if(text.charAt(j) == openChar)
						open++;
					else if(text.charAt(j) == closeChar)
						close++;
					if(open == close) {
						return new Integer[] {i+1, j};
					}
				}
			}
		}
		return new Integer[] {0, 0};
	}

}
