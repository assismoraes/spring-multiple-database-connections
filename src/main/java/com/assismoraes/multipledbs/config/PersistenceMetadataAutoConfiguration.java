package com.assismoraes.multipledbs.config;

import static java.util.Collections.singletonMap;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;

@Configuration
//@PropertySource({"classpath:persistence-multiple-db-boot.properties"})
@EnableJpaRepositories(
  basePackages = "com.assismoraes.multipledbs.metadata", 
  entityManagerFactoryRef = "metadataEntityManager", 
  transactionManagerRef = "metadataTransactionManager")
public class PersistenceMetadataAutoConfiguration {
	
	@Autowired
	private ConfigProperties configProperties;
	
	@Bean(name = "metadata-db")
	//  @ConfigurationProperties(prefix="spring.second-datasource")
	public DataSource productDataSource() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName("org.postgresql.Driver");
		dataSource.setUrl("jdbc:postgresql://localhost:5432/metadata");
		dataSource.setUsername("postgres");
		dataSource.setPassword("postgres");
		return dataSource;
	}

	@Bean(name = "metadataEntityManager")
	public LocalContainerEntityManagerFactoryBean secondEntityManagerFactory(final EntityManagerFactoryBuilder builder,
			final @Qualifier("metadata-db") DataSource dataSource) {
		return builder
				.dataSource(dataSource)
				.packages("com.assismoraes.multipledbs.metadata.models")
				.persistenceUnit("secondDb")
				.properties(singletonMap("hibernate.hbm2ddl.auto", "update"))
				.properties(singletonMap("hibernate.temp.use_jdbc_metadata_defaults", "false"))
				.properties(singletonMap("hibernate.dialect", "org.hibernate.dialect.PostgreSQLDialect"))
				.build();
	}
	@Bean(name = "metadataTransactionManager")
	public PlatformTransactionManager secondTransactionManager(@Qualifier("metadataEntityManager")
	EntityManagerFactory secondEntityManagerFactory) {
		return new JpaTransactionManager(secondEntityManagerFactory);
	}

}
