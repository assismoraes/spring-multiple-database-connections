package com.assismoraes.multipledbs.controllers;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.assismoraes.multipledbs.metadata.models.MetadataTable;
import com.assismoraes.multipledbs.metadata.repositories.MetadataTableRepository;
import com.assismoraes.multipledbs.report.models.Report;
import com.assismoraes.multipledbs.report.repositories.ReportRepository;

@RestController
public class CRUDController {

	@Autowired
	private ReportRepository reportRepo;
	
	@Autowired
	private MetadataTableRepository metadataTableRepo;
	
	@GetMapping("createReport")
	public Report createReport() {
		Report report = new Report();
		report.setName("Relatório 23424");
		report.setQuery("query { reports { id name query createdAt } }");
		report.setCreatedAt(new Date());
		return reportRepo.save(report);
	}
	
	@GetMapping("createMetadataTable")
	public MetadataTable createMetadataTable() {
		MetadataTable metadataTable = new MetadataTable();
		metadataTable.setQueryName("dimBeneficiaries");
		return metadataTableRepo.save(metadataTable);
	}
	
	
	
	
	
	
}
