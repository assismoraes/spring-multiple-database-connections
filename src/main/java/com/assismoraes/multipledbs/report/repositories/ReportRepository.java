package com.assismoraes.multipledbs.report.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.assismoraes.multipledbs.report.models.Report;

@Repository
public interface ReportRepository extends JpaRepository<Report, Long> {

}
