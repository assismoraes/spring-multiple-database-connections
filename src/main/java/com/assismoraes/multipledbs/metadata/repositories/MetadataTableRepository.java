package com.assismoraes.multipledbs.metadata.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.assismoraes.multipledbs.metadata.models.MetadataTable;

@Repository
public interface MetadataTableRepository extends JpaRepository<MetadataTable, Long> {

}
